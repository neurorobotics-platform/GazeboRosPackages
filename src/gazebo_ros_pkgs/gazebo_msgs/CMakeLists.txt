cmake_minimum_required(VERSION 3.0.2)
project(gazebo_msgs)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  nav_msgs
  trajectory_msgs
  geometry_msgs
  sensor_msgs
  std_srvs
  message_generation
  )

add_message_files(
  DIRECTORY msg
  FILES
  ContactsState.msg
  ContactState.msg
  LinkState.msg
  LinkStates.msg
  ModelState.msg
  ModelStates.msg
  ODEJointProperties.msg
  ODEPhysics.msg
  WorldState.msg
# NRP custom messages
  JointStates.msg
  OpenSimPhysics.msg
  WheelSpeeds.msg
  JointsPositions.msg
  )

add_service_files(DIRECTORY srv FILES
  ApplyBodyWrench.srv
  DeleteModel.srv
  DeleteLight.srv
  GetCameraImage.srv
  GetLinkState.srv
  GetPhysicsProperties.srv
  SetJointStates.srv
  SetJointProperties.srv
  SetModelConfiguration.srv
  SpawnEntity.srv
  ApplyJointEffort.srv
  GetJointProperties.srv
  GetJointStates.srv
  GetModelProperties.srv
  GetWorldProperties.srv
  SetLinkProperties.srv
  SetModelState.srv
  BodyRequest.srv
  GetLinkProperties.srv
  GetModelState.srv
  JointRequest.srv
  SetLinkState.srv
  SetPhysicsProperties.srv
  SetJointTrajectory.srv
  GetLightProperties.srv
  SetLightProperties.srv
# NRP custom services
  AdvanceSimulation.srv
  ExportWorldSDF.srv
  GetLightsName.srv
  GetVisualProperties.srv
  SetVisualProperties.srv
  SetSensorNoiseProperties.srv
  GetSensorNoiseProperties.srv
  SetHuskyCmdVel.srv
  GetHuskyOdometry.srv
  GetHuskyJointStates.srv
  SetHuskyWheelSpeeds.srv
  )

generate_messages(DEPENDENCIES
  std_msgs
  geometry_msgs
  nav_msgs
  sensor_msgs
  trajectory_msgs
  )

catkin_package(
  CATKIN_DEPENDS
  message_runtime
  std_msgs
  nav_msgs
  trajectory_msgs
  geometry_msgs
  sensor_msgs
  std_srvs
  )
