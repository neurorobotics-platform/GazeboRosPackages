#include <math.h>
#include <iostream>
#include <boost/math/special_functions/sign.hpp>
#include <std_msgs/Float64.h>
#include "amphibot_simple_fluids_controller.hh"
#include <iomanip>
std::ofstream file;

inline double round(double val)
{
    if (val < 0)
        return ceil(val - 0.5);
    return floor(val + 0.5);
}

void AmphibotController::ReadCSVData(const std::string &filePath)
{

    csvFile.open(filePath);
    if (!csvFile.is_open())
        throw std::runtime_error("Could not open file");

    std::vector<std::vector<double>> data;
    int i = 0;
    while (csvFile)
    {
        std::string s;
        if (!std::getline(csvFile, s))
            break;

        std::istringstream ss(s);
        std::vector<double> record;

        while (ss)
        {
            std::string s;
            if (!getline(ss, s, ','))
                break;
            record.push_back(std::stod(s));
        }
        int index = i;
        record.pop_back();
        std::pair<int, std::vector<double>> result(i, record);
        csvPositions.insert(result);
        i++;
    }
}

void AmphibotController::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
    // Store the pointer to the model
    this->model = _model;

    // Save pointers
    this->world = this->model->GetWorld();

    // Initialize the ROS node for the gazebo client if necessary
    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "lampreyController",
                  ros::init_options::NoSigintHandler);
    }
    rosnode.reset(new ros::NodeHandle());

    // setup a ros topic for the velocity and position targets
    std::string lampreyTargetsTopicName = "/lamprey_targets";
    ros::SubscribeOptions so =
        ros::SubscribeOptions::create<gazebo_msgs::JointsPositions>(lampreyTargetsTopicName, 1,
                                                                    boost::bind(&AmphibotController::OnTargets, this, _1),
                                                                    ros::VoidPtr(), &queue);
    targetsSub = rosnode->subscribe(so);
    model->SaveControllerActuatorRosTopics(lampreyTargetsTopicName, "gazebo_msgs::JointsPositions");

    this->callbackQueueThread =
        std::thread(std::bind(&AmphibotController::QueueThread, this));

    std::string pubTopicName = this->model->GetName() + "/rotation_error";
    rotationErrorPub = rosnode->advertise<std_msgs::Float64>(pubTopicName, 1);

    // Set up PID parameters
    int j = 0;
    for (int i = 0; i < this->model->GetJoints().size(); ++i)
    {
        std::shared_ptr<Joint> jointPtr = std::make_shared<Joint>();
        jointPtr->joint = this->model->GetJoints()[i];
        if (!jointPtr->joint->HasType(gazebo::physics::Joint::FIXED_JOINT))
        {
            std::string jointId = "joint_c2m_" + std::to_string(j);
            auto posP = 10.0;
            auto posI = 0.0;
            auto posD = 0.05;

            auto velP = 0.1;
            auto velI = 0.0;
            auto velD = 0.0;

            this->model->GetJointController()->SetPositionPID(jointPtr->joint->GetScopedName(), common::PID(posP, posI, posD));
            this->model->GetJointController()->SetVelocityPID(jointPtr->joint->GetScopedName(), common::PID(velP, velI, velD));
            joints.push_back(jointPtr);
            j++;
        }
    }

    // Ideal CPG Parameters
    this->amplitude =0.5;
    this->frequency = 2.0;
    this->bias = 0.0;
    this->phase = 1.5;
    this->omega = 2 * M_PI * frequency;

    this->controlMethod = ControlMethod::GazeboPIDControl;
    gzdbg << "Plugin model name: " << this->model->GetName() << "\n";

  }

void AmphibotController::QueueThread()
{
    static const double timeout = 0.01;
    while (rosnode->ok())
    {
        queue.callAvailable(ros::WallDuration(timeout));
    }
}

void AmphibotController::FirstLinkRotationErrorPub(const gazebo_msgs::JointsPositions::ConstPtr &msg)
{
    double currentError = 0;
    if (!firstLinkRotationMemory.empty() && iterations > 501)
        firstLinkRotationMemory.pop_front();

    firstLinkRotationMemory.push_back(this->model->GetLinks()[0]->WorldCoGPose().Rot().Z());
    currentError = msg->positions[msg->positions.size() - 1] - averageFirstLinkRotation;

    double sum = std::accumulate(firstLinkRotationMemory.begin(), firstLinkRotationMemory.end(), 0.0);
    averageFirstLinkRotation = sum / firstLinkRotationMemory.size();

    std_msgs::Float64 errorMessage;

    errorMessage.data = currentError;
    this->rotationErrorPub.publish(errorMessage);
}

void AmphibotController::OnTargets(const gazebo_msgs::JointsPositions::ConstPtr &msg)
{
    boost::mutex::scoped_lock lock(this->mutex);
    common::Time currTime = this->model->GetWorld()->SimTime();
    common::Time stepTime = currTime - this->prevUpdateTime;
    this->prevUpdateTime = currTime;

    if (stepTime.Double() > 0.0)
    {
        // for every joint read (or compute) the target position,  and
        // apply as a target with a control method
        for (int i = 0; i < this->joints.size(); i++)
        {
            auto joint = this->joints[i]->joint;
            double posCmd = 0.0, velCmd = 0.0, currFilteredValue = 0.0;
            auto currPosition = joint->Position(0);
            auto currVelocity = joint->GetVelocity(0);

            // read the target position from the ROS topic
           // posCmd = msg->positions[i];
            // ideal signal
            posCmd = amplitude * sin(omega * currTime.Double() - phase * i) + bias;
            //velCmd = omega * amplitude * cos(omega * currTime.Double() - phase * i);

            // Finite differences scheme of first order for the velocity
            // velCmd = (posCmd - positionsMemory[i]) / stepTime.Double();

            // 4 Control Methods:
            // 1: Gazebo Default PID controller
            // 2: Explicitly setting the joint to a position (works best when setting the velocity as well)
            // 3: Explicitly setting the velocity of the joint (works best when setting the position as well)
            // 4: PD control based on the PID controller equation provided by pybullet
            if (this->controlMethod == ControlMethod::GazeboPIDControl)
            {
                double target = posCmd;
                this->model->GetJointController()->SetPositionTarget(joint->GetScopedName(), target);
            }

            if (this->controlMethod == ControlMethod::directPositionControl)
                joint->SetPosition(0, posCmd, true);

            if (this->controlMethod == ControlMethod::velocityControl)
            {
                joint->SetParam("fmax", 0, 100.0);
                joint->SetParam("vel", 0, velCmd);
            }

            if (this->controlMethod == ControlMethod::PDControl)
            {
                auto forceTarget = this->joints[i]->pidPosition.GetPGain() * ((posCmd - currPosition) / stepTime.Double()) + this->joints[i]->pidPosition.GetDGain() * (velCmd - currVelocity);
                joint->SetForce(0, forceTarget);
            }
        }

        iterations++;
        this->FirstLinkRotationErrorPub(msg);
    }
}

GZ_REGISTER_MODEL_PLUGIN(AmphibotController);
