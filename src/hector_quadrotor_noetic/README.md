## Version

This folder is coming from the repository : https://github.com/RAFALAMAO/hector-quadrotor-noetic, from commit 8d07791 on branch main.
Modifications have been made so that cloning the unique_identifier / geographic_info repositories is not required anymore.
In particular : geometry_msgs have been used in hector_localization instead of geographic_msgs (coming from geographic_info), using same
format but different names (x,y,z instead of longitude, latitude and altitude).

# hector_quadrotor ported to ROS Noetic & Gazebo 11

<img src="imgs/dron_photo.png" height="250"/> <img src="imgs/dron_photo_rviz.png" height="250"/>

***.:: First version, please tell me the issues or help me to fix it ::.***

I took part of this from __The Construct's__ [repo](https://bitbucket.org/theconstructcore/hector_quadrotor_sim/src/master/) and YouTube [chanel](https://www.youtube.com/channel/UCt6Lag-vv25fTX3e11mVY1Q).

## Requirements

I. Clone `hector_quadrotor_noetic`.
```sh
git clone https://github.com/RAFALAMAO/hector_quadrotor_noetic.git
``
II. Build.
```sh
cd ~/catkin_ws && catkin_make
```

## Usage

Run a simulation by executing the launch file in `hector_quadrotor_gazebo` and `hector_quadrotor_demo` packages (only these work at the momment, but you can try the other ones):

* `roslaunch hector_quadrotor_gazebo quadrotor_empty_world.launch`
* `roslaunch hector_quadrotor_demo outdoor_flight_gazebo.launch`
* `roslaunch hector_quadrotor_demo outdoor_flight_gazebo_no_rviz.launch`
* `roslaunch hector_quadrotor_demo two_drones_empty.launch`

You can control it with teleop_twist_keyboard.
* `git clone https://github.com/ros-teleop/teleop_twist_keyboard`

## Test

Here is a [video](https://www.youtube.com/watch?v=-2IWfZjqoNc) testing it:

[![IMAGE ALT TEXT HERE](https://github.com/RAFALAMAO/hector_quadrotor_noetic/blob/main/imgs/gif.GIF)](https://www.youtube.com/watch?v=-2IWfZjqoNc)
